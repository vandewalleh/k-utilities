package be.vandewalleh.gui.components

import be.vandewalleh.graphics.Color.WHITE
import be.vandewalleh.graphics.Point
import be.vandewalleh.kutilities.utils.drawCenteredString
import be.vandewalleh.kutilities.utils.drawTexturedRect
import be.vandewalleh.kutilities.utils.minecraft
import net.minecraft.client.audio.PositionedSoundRecord
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.util.ResourceLocation
import org.lwjgl.opengl.GL11.*

open class Button(var label: String) : Component() {

    init {
        clickListeners += {
            minecraft.soundHandler.playSound(PositionedSoundRecord.create(buttonClick))
        }
    }

    override fun draw() {
        minecraft.textureManager.bindTexture(buttonTextures)
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F)
        GlStateManager.enableBlend()
        GlStateManager.tryBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO)
        GlStateManager.blendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        val i = if (clickable()) when {
            focused -> 2
            else -> 1
        } else 0

        val rect1 = rectangle.copy(width = rectangle.width / 2)
        val texturePos1 = Point(0, 46 + i * 20)
        drawTexturedRect(rect1, texturePos1)

        val rect2 = rectangle.copy(left = rectangle.left + rectangle.width / 2, width = rectangle.width / 2)
        val texturePos2 = Point(200 - rectangle.width / 2, 46 + i * 20)
        drawTexturedRect(rect2, texturePos2)

        drawCenteredString(label, rectangle.left + rectangle.width / 2, rectangle.top + (rectangle.height - 8) / 2, WHITE)
    }

    private companion object {
        val buttonTextures = ResourceLocation("textures/gui/widgets.png")
        val buttonClick = ResourceLocation("gui.button.press")
    }
}