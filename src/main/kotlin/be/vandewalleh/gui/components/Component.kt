package be.vandewalleh.gui.components

import be.vandewalleh.graphics.drawables.Drawable


abstract class Component : Drawable() {
    val clickListeners = mutableListOf<() -> Unit>()
    var keyTypedListener: ((typedChar: Char, keyCode: Int) -> Unit)? = null
    var clickable: () -> Boolean = { true }
    var focused = false

    override fun calculateDimension() {}
}