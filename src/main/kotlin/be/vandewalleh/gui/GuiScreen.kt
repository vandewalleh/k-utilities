package be.vandewalleh.gui

import be.vandewalleh.graphics.drawables.Container

interface GuiScreen {
    var onCloseListener: () -> Unit

    fun setLayout(componentsSupplier: ComponentsSupplier)

    fun goBack()

    fun launch()

    fun exit()
}

class ComponentsSupplier(val supplier: () -> Container) {
    lateinit var guiScreen: GuiScreen
}

