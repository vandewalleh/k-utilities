package be.vandewalleh.gui

import be.vandewalleh.graphics.Alignment.CENTER
import be.vandewalleh.graphics.Point
import be.vandewalleh.graphics.drawables.ScreenContainer
import be.vandewalleh.gui.components.Component
import be.vandewalleh.kutilities.utils.minecraft
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.ScaledResolution
import org.lwjgl.input.Keyboard.KEY_ESCAPE
import java.util.*
import net.minecraft.client.gui.GuiScreen as MinecraftGuiScreen


object GuiScreenFactory {
    fun create(): GuiScreen {
        return GuiScreenImpl()
    }

    private class GuiScreenImpl : MinecraftGuiScreen(), GuiScreen {
        override var onCloseListener: () -> Unit = {}
        private val screenContainer = ScreenContainer(CENTER)
        private var hoveredComponent: Component? = null
        private val lastSuppliersQueue = ArrayDeque<ComponentsSupplier>()
        private var componentsSupplier: ComponentsSupplier? = null
        private var lastComponentsSupplier: ComponentsSupplier? = null

        override fun setLayout(componentsSupplier: ComponentsSupplier) {
            this.componentsSupplier = componentsSupplier
            componentsSupplier.guiScreen = this
            lastComponentsSupplier?.let { lastSuppliersQueue.addFirst(it) }
            lastComponentsSupplier = componentsSupplier
            refreshComponents()
        }

        private fun refreshComponents() {
            screenContainer.clear()
            screenContainer.add(componentsSupplier?.supplier!!.invoke())
        }

        override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
            setFocusedComponent(Point(mouseX, mouseY))
            screenContainer.initDrawing()
            if (!screenContainer.shouldRender()) return
            screenContainer.calculateDimension()
            screenContainer.draw()
        }

        override fun setWorldAndResolution(mc: Minecraft, width: Int, height: Int) {
            super.setWorldAndResolution(mc, width, height)
            screenContainer.setScreenDimension(ScaledResolution(mc))
        }

        override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int) {
            hoveredComponent?.clickListeners?.forEach { it() }
        }

        override fun keyTyped(typedChar: Char, keyCode: Int) {
            hoveredComponent?.keyTypedListener?.invoke(typedChar, keyCode)
            if (keyCode == KEY_ESCAPE) goBack()
        }

        override fun goBack() {
            lastComponentsSupplier = null
            val supplier: ComponentsSupplier? = lastSuppliersQueue.poll()
            if (supplier != null) setLayout(supplier)
            else exit()
        }

        override fun launch() {
            minecraft.displayGuiScreen(this)
        }

        override fun exit() {
            minecraft.displayGuiScreen(null)
        }

        override fun onGuiClosed() {
            onCloseListener()
        }

        private fun setFocusedComponent(mouse: Point) {
            hoveredComponent = null
            screenContainer.allComponents().forEach {
                if (it is Component) {
                    if (setFocused(it, mouse)) hoveredComponent = it
                }
            }
        }

        private fun setFocused(component: Component, mouse: Point): Boolean {
            val focused = component.rectangle.contains(mouse)
            component.focused = focused
            return focused
        }

    }

}
