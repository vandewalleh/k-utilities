package be.vandewalleh.kotlinadapter

import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin

class KotlinPlugin : IFMLLoadingPlugin {
    override fun getASMTransformerClass(): Array<String> {
        return arrayOf()
    }

    override fun getModContainerClass(): String? {
        return null
    }

    override fun getSetupClass(): String {
        return KotlinAdapterLoader::class.java.name
    }

    override fun injectData(data: Map<String, Any>) {

    }

    override fun getAccessTransformerClass(): String? {
        return null
    }
}