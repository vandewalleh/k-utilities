package be.vandewalleh.kotlinadapter

import net.minecraftforge.fml.relauncher.IFMLCallHook

class KotlinAdapterLoader : IFMLCallHook {
    override fun injectData(data: Map<String, Any>) {
        val loader = data["classLoader"] as ClassLoader
        try {
            loader.loadClass(KotlinAdapter::class.java.name)
        } catch (e: ClassNotFoundException) {
            throw RuntimeException(e)
        }

    }

    override fun call(): Void? {
        return null
    }
}