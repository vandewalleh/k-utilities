package be.vandewalleh.kutilities

import be.vandewalleh.kotlinadapter.KotlinAdapter
import be.vandewalleh.kutilities.config.Configuration
import be.vandewalleh.kutilities.gui.ConfigGui
import be.vandewalleh.kutilities.modules.*
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.Mod.EventHandler
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import org.apache.logging.log4j.LogManager
import java.io.File

@Mod(
        modid = KUtilities.NAME,
        version = KUtilities.VERSION,
        name = KUtilities.NAME,
        modLanguageAdapter = KotlinAdapter.PATH,
        acceptedMinecraftVersions = "[1.8.9]",
        clientSideOnly = true,
        useMetadata = true
)
object KUtilities {
    const val NAME = "K-Utilities"
    const val VERSION = "1.0"

    val modules = listOf(AutoSprint(), Perspective(), Fps(), Coordinates())
    val logger = LogManager.getLogger(NAME)!!

    @EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        val directory = File(event.suggestedConfigurationFile.parent, NAME).apply { mkdirs() }
        Configuration.directory = directory
    }

    @EventHandler
    fun init(event: FMLInitializationEvent) {
        ConfigGui
        modules.forEach(Module::init)
        Configuration.load()
    }

}
