package be.vandewalleh.kutilities.modules

import be.vandewalleh.graphics.Alignment
import be.vandewalleh.graphics.drawables.VerticalContainer
import be.vandewalleh.kutilities.OverlayManager
import be.vandewalleh.kutilities.config.PropertyDelegate
import be.vandewalleh.kutilities.config.property


abstract class OverlayModule : Module() {
    var container = VerticalContainer()
    private lateinit var overlayListener: OverlayPropertyListener

    private val alignmentProperty = property<Alignment> {
        name = "alignment"
        value = Alignment.TOP_LEFT
        type = Alignment::class.java
        possibleValues = OverlayManager.alignments
        changeListener = { overlayListener.onToggled(enabled) }
    }

    var alignment: Alignment by PropertyDelegate(alignmentProperty)

    override fun init() {
        super.init()
        overlayListener = OverlayPropertyListener(this)
        add(overlayListener)
        add(alignmentProperty)
        fillContainer(container)
    }

    abstract fun fillContainer(container: VerticalContainer)
}

class OverlayPropertyListener(private val overlayModule: OverlayModule) : ToggleListener {
    override fun onToggled(enabled: Boolean) {
        OverlayManager.remove(overlayModule)
        if (enabled) OverlayManager.add(overlayModule)
    }
}
