package be.vandewalleh.kutilities.modules

import be.vandewalleh.graphics.Color
import be.vandewalleh.graphics.drawables.TextDrawable
import be.vandewalleh.graphics.drawables.VerticalContainer
import net.minecraft.client.Minecraft

class Fps : OverlayModule() {
    override fun fillContainer(container: VerticalContainer) {
        val textDrawable = TextDrawable().apply {
            title = name
            color = Color.BLUE
        }
        container.add(textDrawable)
        container.preInitCallback.add {
            textDrawable.text = Minecraft.getDebugFPS().toString()
        }
    }
}