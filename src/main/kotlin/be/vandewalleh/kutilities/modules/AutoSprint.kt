package be.vandewalleh.kutilities.modules

import be.vandewalleh.kutilities.propertylisteners.EventBusPropertyListener
import be.vandewalleh.kutilities.utils.thePlayer
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent

class AutoSprint : Module() {
    override fun init() {
        super.init()
        add(EventBusPropertyListener(this))
    }

    @SubscribeEvent
    fun sprint(event: ClientTickEvent) {
        thePlayer()?.let {
            val shouldSprint =
                    !it.isSprinting
                            && !it.isCollidedHorizontally
                            && it.moveForward > 0
                            && !it.isSneaking

            if (shouldSprint) it.isSprinting = true
        }
    }

}