package be.vandewalleh.kutilities.modules

import be.vandewalleh.kutilities.keybinding.ActionableKey
import be.vandewalleh.kutilities.modules.PerspectiveEnum.BACK
import be.vandewalleh.kutilities.modules.PerspectiveEnum.FIRST
import be.vandewalleh.kutilities.propertylisteners.ActionableKeyPropertyListener
import be.vandewalleh.kutilities.utils.gameSettings
import org.lwjgl.input.Keyboard.KEY_F

class Perspective : Module() {
    private val actionableKey = ActionableKey(
            name,
            KEY_F,
            onKeyDown = { setPerspective(BACK) },
            onKeyRelease = { setPerspective(FIRST) }
    )

    private fun setPerspective(perspective: PerspectiveEnum) {
        gameSettings.thirdPersonView = perspective.thirdPersonView
    }

    override fun init() {
        super.init()
        add(ActionableKeyPropertyListener(actionableKey))
    }
}

enum class PerspectiveEnum(val thirdPersonView: Int) {
    FIRST(0), THIRD(1), BACK(2);
}
