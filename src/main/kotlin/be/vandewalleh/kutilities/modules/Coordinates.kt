package be.vandewalleh.kutilities.modules

import be.vandewalleh.graphics.Color
import be.vandewalleh.graphics.drawables.TextDrawable
import be.vandewalleh.graphics.drawables.VerticalContainer
import be.vandewalleh.kutilities.utils.thePlayer

class Coordinates : OverlayModule() {
    override fun fillContainer(container: VerticalContainer) {
        Axis.values().forEach {
            val textDrawable = TextDrawable().apply {
                title = it.name
                color = Color.GOLD
            }
            container.preInitCallback.add { textDrawable.text = getAxisText(it) }
            container.add(textDrawable)
        }
    }

    private fun getAxisText(axis: Axis): String {
        val thePlayer = thePlayer()!!
        val pos = when (axis) {
            Axis.X -> thePlayer.posX
            Axis.Y -> thePlayer.posY
            Axis.Z -> thePlayer.posZ
        }
        return pos.toInt().toString()
    }

    enum class Axis {
        X, Y, Z
    }
}