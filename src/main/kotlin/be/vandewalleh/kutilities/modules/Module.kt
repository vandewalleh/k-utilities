package be.vandewalleh.kutilities.modules

import be.vandewalleh.kutilities.config.Property
import be.vandewalleh.kutilities.config.PropertyDelegate
import be.vandewalleh.kutilities.config.booleanProperty

abstract class Module {
    val name = javaClass.simpleName!!
    private val toggleListeners = mutableListOf<ToggleListener>()
    private val propertiesByName = mutableMapOf<String, Property<*>>()

    private val enabledProperty = booleanProperty {
        name = "enabled"
        value = true
        showInGui = false
        changeListener = { value -> toggleListeners.forEach { it.onToggled(value) } }
    }

    var enabled: Boolean by PropertyDelegate(enabledProperty)

    open fun init() {
        add(enabledProperty)
    }

    fun add(property: Property<*>) {
        propertiesByName[property.name] = property
    }

    fun add(toggleListener: ToggleListener) {
        toggleListeners.add(toggleListener)
    }

    fun propertiesByName() = propertiesByName.toMap()

}

interface ToggleListener {
    fun onToggled(enabled: Boolean)
}