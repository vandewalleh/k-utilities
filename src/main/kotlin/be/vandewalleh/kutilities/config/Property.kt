package be.vandewalleh.kutilities.config

import com.google.gson.GsonBuilder
import java.lang.reflect.Type
import kotlin.properties.Delegates

class Property<T : Any>(
        val name: String,
        private val type: Type,
        value: T,
        val possibleValues: List<T>,
        val showInGui: Boolean,
        private val changeListener: (T) -> Unit
) {
    var value: T by Delegates.observable(value) { _, _, newValue -> changeListener(newValue) }

    fun deserialize(value: String) {
        this.value = GSON.fromJson(value, type)
    }

    fun serialize(): String {
        return GSON.toJson(value, type)
    }

    companion object {
        private val GSON = GsonBuilder().setPrettyPrinting().create()!!
    }
}

