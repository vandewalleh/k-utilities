package be.vandewalleh.kutilities.config

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class PropertyDelegate<T : Any>(private val property: Property<T>) : ReadWriteProperty<Any?, T> {
    override fun getValue(thisRef: Any?, unused: KProperty<*>): T {
        return property.value
    }

    override fun setValue(thisRef: Any?, unused: KProperty<*>, value: T) {
        property.value = value
    }
}