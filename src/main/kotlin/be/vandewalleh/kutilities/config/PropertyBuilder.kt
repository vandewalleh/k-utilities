package be.vandewalleh.kutilities.config

import java.lang.reflect.Type

open class PropertyBuilder<T : Any> {
    lateinit var name: String
    lateinit var type: Type
    lateinit var value: T
    lateinit var possibleValues: List<T>
    var showInGui: Boolean = true
    var changeListener: (T) -> Unit = {}

    fun build() = Property(name, type, value, possibleValues, showInGui, changeListener)
}

fun <T : Any> property(init: PropertyBuilder<T>.() -> Unit): Property<T> {
    return PropertyBuilder<T>().apply { init() }.build()
}


class BooleanPropertyBuilder : PropertyBuilder<Boolean>() {
    init {
        type = BooleanPropertyBuilder.type
        possibleValues = BooleanPropertyBuilder.possibleValues
    }

    companion object {
        private val type = Boolean::class.java
        private val possibleValues = listOf(true, false)
    }
}

fun booleanProperty(init: BooleanPropertyBuilder.() -> Unit): Property<Boolean> {
    return BooleanPropertyBuilder().apply { init() }.build()
}
