package be.vandewalleh.kutilities.config

import be.vandewalleh.kutilities.KUtilities
import be.vandewalleh.kutilities.modules.Module
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.io.FileNotFoundException


object Configuration {
    lateinit var directory: File
    private val GSON = GsonBuilder().setPrettyPrinting().create()!!
    private val propertiesMapType = object : TypeToken<Map<String, String>>() {}.type!!

    fun load() {
        KUtilities.logger.info("Loading configuration")
        KUtilities.modules.forEach {
            try {
                val json = it.configFile().readText()
                val valuesByName = GSON.fromJson<Map<String, String>>(json, propertiesMapType)
                valuesByName.forEach { name, value ->
                    it.propertiesByName()[name]?.deserialize(value)
                }
            } catch (e: FileNotFoundException) {
                it.enabled = it.enabled //invoke property change listeners
                KUtilities.logger.info("File not found for ${it.name} module, using default")
            }
        }
    }

    fun save() {
        KUtilities.logger.info("Saving configuration")
        KUtilities.modules.forEach {
            val valuesByName = it.propertiesByName()
                    .values
                    .associateBy(Property<*>::name, Property<*>::serialize)

            val json = GSON.toJson(valuesByName)
            it.configFile().writeText(json)
        }
    }

    private fun Module.configFile(): File {
        return File(directory, this.name + ".json")
    }
}
