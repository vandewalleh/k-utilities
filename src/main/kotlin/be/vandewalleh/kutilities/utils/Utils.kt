package be.vandewalleh.kutilities.utils

import be.vandewalleh.graphics.Color
import be.vandewalleh.graphics.Point
import be.vandewalleh.graphics.Rectangle
import be.vandewalleh.kutilities.KUtilities
import net.minecraft.client.Minecraft
import net.minecraft.client.entity.EntityPlayerSP
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.Gui
import net.minecraft.client.settings.GameSettings
import net.minecraft.client.settings.KeyBinding
import org.apache.commons.lang3.ArrayUtils

val minecraft: Minecraft by lazy { Minecraft.getMinecraft() }

val fontRenderer: FontRenderer by lazy { minecraft.fontRendererObj }

fun thePlayer(): EntityPlayerSP? = minecraft.thePlayer

val gameSettings: GameSettings by lazy { minecraft.gameSettings }

val gui: Gui by lazy { Gui() }

fun drawString(text: String, x: Int, y: Int, color: Color = Color.WHITE) {
    fontRenderer.drawString(text, x.toFloat(), y.toFloat(), color.colorCode, true)
}

fun drawCenteredString(text: String, left: Int, top: Int, color: Color = Color.WHITE) {
    drawString(text, left - fontRenderer.getStringWidth(text) / 2, top, color)
}

fun drawTexturedRect(rectangle: Rectangle, point: Point) {
    gui.drawTexturedModalRect(rectangle.left, rectangle.top, point.x, point.y, rectangle.width, rectangle.height)
}

fun GameSettings.addKeyBinding(keyBinding: KeyBinding) {
    this.keyBindings = ArrayUtils.add(this.keyBindings, keyBinding)
}

fun GameSettings.removeKeyBinding(keyBinding: KeyBinding) {
    this.keyBindings = ArrayUtils.removeElement(this.keyBindings, keyBinding)
}

fun newKeyBinding(description: String, keyCode: Int = 0): KeyBinding {
    return KeyBinding(description, keyCode, KUtilities.NAME)
}
