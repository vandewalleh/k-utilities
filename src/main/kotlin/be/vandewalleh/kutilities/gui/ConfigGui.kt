package be.vandewalleh.kutilities.gui

import be.vandewalleh.graphics.Color
import be.vandewalleh.graphics.drawables.GridComponentsContainer
import be.vandewalleh.gui.ComponentsSupplier
import be.vandewalleh.gui.GuiScreenFactory
import be.vandewalleh.gui.components.Button
import be.vandewalleh.kutilities.KUtilities
import be.vandewalleh.kutilities.config.Configuration
import be.vandewalleh.kutilities.keybinding.ActionableKey
import be.vandewalleh.kutilities.keybinding.KeyHandler
import be.vandewalleh.kutilities.modules.Module
import org.lwjgl.input.Keyboard.KEY_RSHIFT

object ConfigGui {
    init {
        KeyHandler.add(ActionableKey("Open Gui", KEY_RSHIFT, onKeyPress = { launch() }))
    }

    private fun launch() {
        GuiScreenFactory.create().apply {
            onCloseListener = { Configuration.save() }
            setLayout(ComponentsSupplier { createComponentsSupplier() })
        }.launch()
    }

    private fun createComponentsSupplier() = GridComponentsContainer().apply {
        KUtilities.modules.forEach {
            val button = Button(buttonLabel(it)).apply {
                clickListeners += {
                    it.enabled = !it.enabled
                    label = buttonLabel(it)
                }
            }
            add(button)
        }
    }

    private fun buttonLabel(module: Module): String {
        return if (module.enabled) Color.GREEN.formattingCode + module.name
        else Color.RED.formattingCode + module.name
    }


}