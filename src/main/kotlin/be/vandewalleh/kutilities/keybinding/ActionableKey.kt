package be.vandewalleh.kutilities.keybinding

import be.vandewalleh.kutilities.utils.newKeyBinding

class ActionableKey(
        description: String,
        keyCode: Int = 0,
        private val onKeyPress: () -> Unit = {},
        private val onKeyDown: () -> Unit = {},
        private val onKeyRelease: () -> Unit = {}
) {
    val keyBinding = newKeyBinding(description, keyCode)
    private var wasKeyDown = false

    fun onTick() {
        if (keyBinding.isPressed) {
            onKeyPress()
        }
        if (keyBinding.isKeyDown && !wasKeyDown) {
            onKeyDown()
        } else if (!keyBinding.isKeyDown && wasKeyDown) {
            onKeyRelease()
        }
        wasKeyDown = keyBinding.isKeyDown
    }
}

