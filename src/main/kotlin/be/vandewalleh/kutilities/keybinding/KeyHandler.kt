package be.vandewalleh.kutilities.keybinding

import be.vandewalleh.kutilities.utils.addKeyBinding
import be.vandewalleh.kutilities.utils.gameSettings
import be.vandewalleh.kutilities.utils.removeKeyBinding
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent

object KeyHandler {
    private val actionableKeys = mutableSetOf<ActionableKey>()

    init {
        MinecraftForge.EVENT_BUS.register(this)
    }

    @SubscribeEvent
    fun actionKeys(event: KeyInputEvent) {
        actionableKeys.forEach { it.onTick() }
    }

    fun add(actionableKey: ActionableKey) {
        if (actionableKeys.add(actionableKey)) {
            gameSettings.addKeyBinding(actionableKey.keyBinding)
        }
    }

    fun remove(actionableKey: ActionableKey) {
        if (actionableKeys.remove(actionableKey)) {
            gameSettings.removeKeyBinding(actionableKey.keyBinding)
        }
    }

}
