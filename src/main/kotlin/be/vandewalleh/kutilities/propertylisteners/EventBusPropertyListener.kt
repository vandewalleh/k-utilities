package be.vandewalleh.kutilities.propertylisteners

import be.vandewalleh.kutilities.modules.ToggleListener
import net.minecraftforge.common.MinecraftForge

class EventBusPropertyListener(private val registrable: Any) : ToggleListener {
    override fun onToggled(enabled: Boolean) {
        if (enabled) MinecraftForge.EVENT_BUS.register(registrable)
        else MinecraftForge.EVENT_BUS.unregister(registrable)
    }
}