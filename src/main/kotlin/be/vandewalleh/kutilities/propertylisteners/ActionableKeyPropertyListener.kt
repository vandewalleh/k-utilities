package be.vandewalleh.kutilities.propertylisteners

import be.vandewalleh.kutilities.keybinding.ActionableKey
import be.vandewalleh.kutilities.keybinding.KeyHandler
import be.vandewalleh.kutilities.modules.ToggleListener

class ActionableKeyPropertyListener(private val actionableKey: ActionableKey) : ToggleListener {
    override fun onToggled(enabled: Boolean) {
        if (enabled) KeyHandler.add(actionableKey)
        else KeyHandler.remove(actionableKey)
    }
}

