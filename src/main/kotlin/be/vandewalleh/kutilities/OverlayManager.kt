package be.vandewalleh.kutilities

import be.vandewalleh.graphics.Alignment
import be.vandewalleh.graphics.Alignment.*
import be.vandewalleh.graphics.drawables.ScreenContainer
import be.vandewalleh.kutilities.modules.OverlayModule
import be.vandewalleh.kutilities.utils.gameSettings
import net.minecraftforge.client.event.RenderGameOverlayEvent
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent

object OverlayManager {
    val alignments = Alignment.values().filter { it != CENTER && it != BOTTOM_LEFT && it != BOTTOM_CENTER }
    private val containersByAlignment = alignments.map(::ScreenContainer).associateBy(ScreenContainer::alignment)

    init {
        MinecraftForge.EVENT_BUS.register(this)
    }

    @SubscribeEvent
    fun renderModules(event: RenderGameOverlayEvent) {
        if (event.type != ElementType.HOTBAR || gameSettings.showDebugInfo) return

        containersByAlignment.values.forEach {
            it.run {
                initDrawing()
                if (!shouldRender()) return@forEach
                setScreenDimension(event.resolution)
                calculateDimension()
                draw()
            }
        }
    }

    fun add(overlayModule: OverlayModule) {
        containersByAlignment[overlayModule.alignment]?.add(overlayModule.container)
        overlayModule.container.alignment = overlayModule.alignment
    }

    fun remove(overlayModule: OverlayModule) {
        containersByAlignment.values.forEach { it.remove(overlayModule.container) }
    }

}