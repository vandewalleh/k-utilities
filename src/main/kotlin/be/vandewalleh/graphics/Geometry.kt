package be.vandewalleh.graphics

data class Point(val x: Int = 0, val y: Int = 0)

data class Dimension(val width: Int = 0, val height: Int = 0)

data class Rectangle(var left: Int = 0, var top: Int = 0, var width: Int = 0, var height: Int = 0) {
    val right get() = left + width
    val bottom get() = top + height
    val topLeft get() = Point(left, top)
    val topRight get() = Point(right, top)
    val bottomLeft get() = Point(left, bottom)
    val bottomRight get() = Point(right, bottom)

    fun contains(point: Point): Boolean {
        return point.x in left..right && point.y in top..bottom
    }

}