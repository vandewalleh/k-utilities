package be.vandewalleh.graphics

enum class Color(index: Int) {
    BLACK(0),
    DARK_BLUE(1),
    DARK_GREEN(2),
    DARK_AQUA(3),
    DARK_RED(4),
    DARK_PURPLE(5),
    GOLD(6),
    GRAY(7),
    DARK_GRAY(8),
    BLUE(9),
    GREEN(10),
    AQUA(11),
    RED(12),
    LIGHT_PURPLE(13),
    YELLOW(14),
    WHITE(15);

    val formattingCode: String
    val colorCode: Int

    init {
        val hex = Integer.toHexString(index)
        formattingCode = "\u00a7" + hex

        val l = (index shr 3 and 1) * 85
        var r = (index shr 2 and 1) * 170 + l
        val g = (index shr 1 and 1) * 170 + l
        val b = (index and 1) * 170 + l

        if (index == 6) {
            r += 85
        }

        colorCode = r and 255 shl 16 or (g and 255 shl 8) or (b and 255)
    }

}