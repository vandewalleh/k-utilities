package be.vandewalleh.graphics

import be.vandewalleh.graphics.HorizontalAlignment.*
import be.vandewalleh.graphics.VerticalAlignment.*

enum class Alignment(val horizontal: HorizontalAlignment, val vertical: VerticalAlignment) {
    TOP_LEFT(LEFT, TOP),
    TOP_CENTER(CENTERH, TOP),
    TOP_RIGHT(RIGHT, TOP),
    CENTER_LEFT(LEFT, CENTERV),
    CENTER(CENTERH, CENTERV),
    CENTER_RIGHT(RIGHT, CENTERV),
    BOTTOM_LEFT(LEFT, BOTTOM),
    BOTTOM_CENTER(CENTERH, BOTTOM),
    BOTTOM_RIGHT(RIGHT, BOTTOM)
}

enum class VerticalAlignment {
    TOP, CENTERV, BOTTOM
}

enum class HorizontalAlignment {
    LEFT, CENTERH, RIGHT
}


