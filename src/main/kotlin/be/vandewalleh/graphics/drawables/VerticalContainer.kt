package be.vandewalleh.graphics.drawables

import be.vandewalleh.graphics.Alignment
import be.vandewalleh.graphics.Alignment.CENTER
import be.vandewalleh.graphics.HorizontalAlignment.*

open class VerticalContainer(alignment: Alignment = CENTER) : Container(alignment = alignment) {
    override fun calculateDimension() {
        drawables.forEach(Drawable::calculateDimension)

        val filteredDrawables = drawables.filter { it.shouldRender() }

        val width = filteredDrawables.map { it.rectangle.width }.max()
        val heightWithoutSpacing = filteredDrawables.map { it.rectangle.height }.sum()

        val spacingCount = filteredDrawables.map { it.rectangle.height }
                .filter { it != 0 }
                .count()

        val verticalSpacing = spacingCount * (spacingCount - 1) * spacing

        rectangle.width = width ?: 0
        rectangle.height = heightWithoutSpacing + verticalSpacing
    }

    override fun draw() {
        val x = rectangle.left
        var y = rectangle.top
        drawables.forEach {
            if (!it.shouldRender()) return@forEach

            it.rectangle.left = x + when (alignment.horizontal) {
                LEFT -> 0
                CENTERH -> (rectangle.width - it.rectangle.width) / 2
                RIGHT -> rectangle.width - it.rectangle.width
            }
            it.rectangle.top = y
            it.draw()

            y += it.rectangle.height + spacing
        }
    }

}
