package be.vandewalleh.graphics.drawables


class GridComponentsContainer : Container(5) {
    private val itemsPerRow = 2
    private val componentHeight = 20
    private val lineWidth = 250
    private val widthWithoutSpacing = lineWidth - (itemsPerRow - 1) * spacing
    private val componentWidth = widthWithoutSpacing / itemsPerRow

    override fun calculateDimension() {
        drawables.forEach {
            it.rectangle.width = componentWidth
            it.rectangle.height = componentHeight
        }
        val lines = drawables.size / itemsPerRow + if (drawables.size % itemsPerRow == 0) 0 else 1
        val height = lines * componentHeight + (lines - 1) * spacing
        rectangle.width = lineWidth
        rectangle.height = height
    }

    override fun draw() {
        val lastLineCount = drawables.size % itemsPerRow
        val lastLineWidth = lastLineCount * componentWidth + (lastLineCount - 1) * spacing
        val lastLineOffset = (lineWidth - lastLineWidth) / 2
        val normalCount = drawables.size - lastLineCount

        var y = rectangle.top
        var rowIndex = 0
        var i = 0

        drawables.forEach {
            var x = rectangle.left + rowIndex * (componentWidth + spacing)
            if (i >= normalCount) x += lastLineOffset

            it.rectangle.left = x
            it.rectangle.top = y
            it.draw()

            rowIndex = if (rowIndex == itemsPerRow - 1) 0 else rowIndex + 1
            if (rowIndex == 0) y += spacing + componentHeight

            i++
        }
    }

}