package be.vandewalleh.graphics.drawables

import be.vandewalleh.graphics.Alignment
import be.vandewalleh.graphics.Alignment.TOP_LEFT
import be.vandewalleh.graphics.Rectangle


abstract class Drawable(var alignment: Alignment = TOP_LEFT) {
    val rectangle = Rectangle()

    open fun initDrawing() {}

    open fun shouldRender(): Boolean {
        return true
    }

    abstract fun calculateDimension()

    abstract fun draw()
}