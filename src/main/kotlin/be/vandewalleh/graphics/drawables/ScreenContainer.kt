package be.vandewalleh.graphics.drawables

import be.vandewalleh.graphics.Alignment
import be.vandewalleh.graphics.Dimension
import be.vandewalleh.graphics.HorizontalAlignment.CENTERH
import be.vandewalleh.graphics.HorizontalAlignment.RIGHT
import be.vandewalleh.graphics.VerticalAlignment.BOTTOM
import be.vandewalleh.graphics.VerticalAlignment.CENTERV
import net.minecraft.client.gui.ScaledResolution

class ScreenContainer(alignment: Alignment) : VerticalContainer(alignment) {
    private var screenDimension = Dimension()

    fun setScreenDimension(scaledResolution: ScaledResolution) {
        screenDimension = Dimension(scaledResolution.scaledWidth - 2, scaledResolution.scaledHeight - 2)
    }

    override fun initDrawing() {
        drawables.forEach { it.alignment = alignment }
        super.initDrawing()
    }

    override fun draw() {
        var left = 1
        var top = 1

        when (alignment.horizontal) {
            CENTERH -> left += screenDimension.width / 2 - rectangle.width / 2
            RIGHT -> left += screenDimension.width - rectangle.width
        }

        when (alignment.vertical) {
            CENTERV -> top += screenDimension.height / 2 - rectangle.height / 2
            BOTTOM -> top += screenDimension.height - rectangle.height
        }

        rectangle.left = left
        rectangle.top = top

        super.draw()
    }


}