package be.vandewalleh.graphics.drawables

import be.vandewalleh.graphics.Alignment
import be.vandewalleh.graphics.Alignment.CENTER

abstract class Container(var spacing: Int = 1, alignment: Alignment = CENTER) : Drawable(alignment) {
    val drawables = mutableListOf<Drawable>()
    var preInitCallback = mutableListOf<() -> Unit>()

    override fun initDrawing() {
        preInitCallback.forEach { it() }
        drawables.forEach(Drawable::initDrawing)
    }

    override fun shouldRender(): Boolean {
        return drawables.isNotEmpty() && drawables.stream().anyMatch(Drawable::shouldRender)
    }

    fun add(drawable: Drawable) {
        drawables.add(drawable)
    }

    fun remove(drawable: Drawable) {
        drawables.remove(drawable)
    }

    fun clear() {
        drawables.clear()
    }

    fun allComponents(): List<Drawable> {
        return allComponents(drawables)
    }

    private fun allComponents(drawables: List<Drawable>): List<Drawable> {
        val list = mutableListOf<Drawable>()
        drawables.forEach {
            if (it is Container) list.addAll(allComponents(it.drawables))
            else list.add(it)
        }
        return list
    }

}