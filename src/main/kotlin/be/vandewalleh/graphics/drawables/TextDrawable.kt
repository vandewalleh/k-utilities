package be.vandewalleh.graphics.drawables

import be.vandewalleh.graphics.Color
import be.vandewalleh.kutilities.utils.drawString
import be.vandewalleh.kutilities.utils.fontRenderer

class TextDrawable : Drawable() {
    lateinit var title: String
    lateinit var text: String
    lateinit var result: String
    lateinit var color: Color

    override fun initDrawing() {
        result = "${color.formattingCode}$title: ${Color.WHITE.formattingCode}$text"
    }

    override fun calculateDimension() {
        rectangle.width = fontRenderer.getStringWidth(result)
        rectangle.height = fontRenderer.FONT_HEIGHT
    }

    override fun draw() {
        drawString(result, rectangle.left, rectangle.top)
    }


}